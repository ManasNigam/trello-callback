const fs = require("fs")
const path = require("path")

function callback1(boardId, callback) {
    setTimeout(() => {


        fs.readFile(path.join(__dirname, "boards.json"), 'utf8', function (err, data) {
            if (err) {
                //if any error occured while execution we call callback
                callback(err);
            }
            else {

                data = JSON.parse(data);

                let getInfoOfBoardMembers = data.filter(items => {
                    return items.id === boardId;

                })
                if (getInfoOfBoardMembers.length === 0) {
                    console.error("Error occured : ", err);
                }
                else {
                    callback(err, getInfoOfBoardMembers);
                }

            }
        })
    }, 2000);
}


module.exports = callback1;