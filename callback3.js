const fs = require("fs")
const path = require("path")

function callback3(listId, callback) {
    setTimeout(() => {

        fs.readFile(path.join(__dirname, "cards.json"), "utf8", (err, data) => {
            if (err) {
                callback(err)
            }
            else {
                data = JSON.parse(data);
                if (!data[listId]) {
                    console.error("Error occured : ", err);
                }
                else {
                    callback(err, data[listId]);
                }
            }
        })


    }, 2000);
}

module.exports = callback3;