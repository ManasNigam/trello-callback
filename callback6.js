
const callback1 = require("./callback1")
const callback2 = require("./callback2")
const callback3 = require("./callback3")
const fs = require("fs")
const cards = require("./cards.json")


function callback5() {
    setTimeout(() => {
        callback1("abc122dc", (err, data) => {
            if (err) {
                console.error("Error occured : ", err);
            }
            console.log(data);
        })
        callback2("abc122dc", (err, data) => {
            if (err) {
                console.error("Error occured : ", err);
            }
            console.log(data);
        })


        fs.readFile("lists.json", "utf8", (err, data) => {
            if (err) {
                console.error("Error : ", err);
            }
            else {
                data = JSON.parse(data);



                for (let index1 = 0; index1 < Object.keys(data).length; index1++) {
                    for (let index2 = 0; index2 < data[(Object.keys(data)[index1])].length; index2++) {
                        if (Object.keys(cards).includes(data[(Object.keys(data)[index1])][index2]["id"])) {

                            let getIdofList = (data[(Object.keys(data)[index1])][index2]["id"]);

                            callback3(getIdofList, (err, data) => {
                                if (err) {
                                    console.error("Error occured : ", err);
                                }
                                else {
                                    console.log(data);
                                }
                            })
                        }
                    }
                }
            }
        })
    }, 2000);
}


module.exports = callback5;
