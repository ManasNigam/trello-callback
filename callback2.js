const fs = require("fs")
const path = require("path")

function callback2(boardId, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname, "lists.json"), 'utf8', function (err, data) {
            if (err) {
                callback(err);
            }
            else {
                data = JSON.parse(data);
                if (!data[boardId]) {
                    console.error("Error occured : ",err);
                } else {
                    callback(err, data[boardId])
                }
            }
        })
    }, 2000);
}

module.exports = callback2;