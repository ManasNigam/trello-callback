const path = require("path");
const callback1 = require(path.join(__dirname,"..","callback1.js"))

callback1("abc122dc", (err, data) => {
    if (err) {
        console.error("Error : ", err);
    } else {
        console.log(data);
    }
})